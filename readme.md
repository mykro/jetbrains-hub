# mykro/jetbrains-hub
A build on top of Debian 8
For information on Hub check out: https://www.jetbrains.com/hub/

## Image Sizes
[![](https://badge.imagelayers.io/mykro/jetbrains-hub:1.0.770.svg)](https://imagelayers.io/?images=mykro/jetbrains-hub:1.0.770) 1.0.770, latest
[![](https://badge.imagelayers.io/mykro/jetbrains-hub:1.0.749.svg)](https://imagelayers.io/?images=mykro/jetbrains-hub:1.0.749) 1.0.749

## Docker Switches
### -p --port
This image runs on port 8080 and assumes being linked into a loadbalancer or reverse proxy such as Nginx. To ensure porting out the correct port use -p HOST_PORT:DOCKER_PORT.
```sh
$ docker run -d -p 10080:8080 mykro/jetbrains-hub
```

### -v --volumes
To enable data persistence, make sure all folders created have been chown'ed to docker:docker.

#### Root home folder
```sh
$ docker run -d -v /HOST/FOLDER/:/jetbrains/hub/ mykro/jetbrains-hub
```

#### Backup folder mapping
```sh
$ docker run -d -v /HOST/FOLDER/backup:/jetbrains/hub/backup mykro/jetbrains-hub
```

#### Data Folder
```sh
$ docker run -d -v /HOST/FOLDER/data:/jetbrains/hub/data mykro/jetbrains-hub
```

#### Log Folder: 
```sh
$ docker run -d -v /HOST/FOLDER/logs:/jetbrains/hub/logs mykro/jetbrains-hub
```

#### Temp Folder 
```sh
$ docker run -d -v /jetbrains/hub/tmp:/jetbrains/hub/tmp mykro/jetbrains-hub
```

### --name
This is a nice switch which allows you to name an instance. This is handy if you're running High Availability with multiple containers mapped to the same host folders. This way it's easier to define the different containers under docker stats, and start/stop commands. 
For example:
```sh
$ docker run -d --name=jbyt-001 -p 8081:8080 mykro/jetbrains-hub
$ docker run -d --name=jbyt-002 -p 8082:8080 mykro/jetbrains-hub
$ docker stop jbyt-001
$ docker start jbyt-001
$ docker stats jbyt-001 jbyt-002
```

### --restart
If in event of container failure, you can get docker to auto-restart by using --restart=always
For example:
```sh
$ docker run -d --restart=always mykro/jetbrains-hub
```

## Running Images
So, putting it all together you'll end up with a command as follows. As mentioned above, ensure that docker has permissions on the host folders otherwise it will fail.
```sh
$ docker run -d \
   -p 1001:8080 \
   -v /HOST/FOLDER/:/jetbrains/hub/ \
   -v /HOST/FOLDER/backup:/jetbrains/hub/backup \
   -v /HOST/FOLDER/data:/jetbrains/hub/data \
   -v /HOST/FOLDER/logs:/jetbrains/hub/logs \
   -v /HOST/FOLDER/tmp:/jetbrains/hub/tmp \
   --name=jbyt-001 \
   --restart=always \
   mykro/jetbrains-hub
```

## Building a custom image
This image depends on the Server runtime version of Java I have built in mykro/java8-jre. This can be pulled with
```sh
$ docker pull mykro/java8-jre:latest
```
Make your edits under the build file, namely the location of files within the container. I have added an Environment Variable that you can change to specify how much RAM you are allowing Java to consume when running. Currently, it is set in the build file as 1024MBs.

## Pulling different versions
Pull a different image with: 
```sh
$ docker pull mykro/jetbrains-hub:[latest|6.5.16981]
```
To see more versions, check the image tag tab above!

## Bugs/Issues/Updates
If you've found a bug/issue, you can find the repo @ https://bitbucket.org/mykro/jetbrains-hub/overview