#!/usr/bin/env bash
docker run -d \
   -p 10101:8080 \
   -v /jetbrains/hub/:/jetbrains/hub/ \
   -v /jetbrains/hub/backup:/jetbrains/hub/backup \
   -v /jetbrains/hub/data:/jetbrains/hub/data \
   -v /jetbrains/hub/logs:/jetbrains/hub/logs \
   -v /jetbrains/hub/tmp:/jetbrains/hub/tmp \
   --name=jb-hub-001 \
   --restart=always \
   mykro/jetbrains-hub
